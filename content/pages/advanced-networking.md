title: Advanced Networking
slug: docs/advanced-networking
status: hidden

## Static Networking

Information for the `MSO Intranet` networking:

Addresses are assigned from these ranges:

```
IPv4: 172.19.134.0/24
IPv6: 2620:101:f000:4903::/64
```

Network gateways:

```
IPv4 Gateway: 172.19.134.254 (NAT gateway)
	Add routes to campus:
		10.0.0.0/8 via 172.19.134.1
		129.97.0.0/16 via 172.19.134.1
		172.16.0.0/12 via 172.19.134.1

		Please do not send on-campus traffic through the NAT gateway.
		This may be blocked in the future.

IPv6 Gateway: 2620:101:f000:4903::1
```

## Firewall

### Campus Firewall

All machines in the CSC cloud are behind the Campus Firewall. The Campus
Firewall blocks all incoming traffic at the University border.

**We will not accept requests for exceptions to the campus firewall. All
inbound traffic originating from off-campus will be blocked.**

### OpenStack Security Groups

You can utilize OpenStack Security Groups to act as a firewall in front of your
host.

*More documentation about this will come at a later time.*

## Private Networking

See [OpenStack Horizon Documentation](https://docs.openstack.org/horizon/pike/user/create-networks.html) for
information about creating private networks.

### Disabling Port Security

Disabling port security is required if you are running a router on your network
(ie. traffic with source IPs different than your own).

*This cannot be done for the MSO Intranet network, however can be done for
private networks*.

This can only be done via the OpenStack command line and the API:
`openstack port set --disable-port-security --no-security-group [portid]`

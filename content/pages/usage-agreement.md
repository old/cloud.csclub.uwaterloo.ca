title: Cloud (Beta) Usage Agreement
slug: usage-agreement

*This agreement, in its current form, applies to those participating in the CSC
Cloud Beta. This agreement may change at any time, with notice, during the beta
period and may change prior to general availability.*

## Introduction

The use of this system is governed by the Computer Science Club's [Machine
Usage Agreement](https://csclub.uwaterloo.ca/services/machine_usage)
and [Code of Conduct](https://csclub.uwaterloo.ca/about/code-of-conduct).
Please take the time to review these documents before using this system. The
Computer Science Club Executive and Systems Committee are responsible for
enforcing these policies.

This agreement is supplemental to the [Machine Usage
Agreement](https://csclub.uwaterloo.ca/services/machine_usage). It
acts primarily to emphasize certain points, but may add additional policies
specific to the Computer Science Club Cloud.

## Use

You may use this service for any purpose, except:

1. Commericial purposes (ex. ads, your startup, etc.)
2. Violation of any usage agreement, University policy, or law

### Eligibility

You may only use this service if:

1. Your membership is current, or
2. If you are a club representative, where use is for club purposes only

### Email

Under no circumstances shall unsolicited (ie. spam) email be sent using this
service.

## Service Level Agreement

### Uptime

> **The Computer Science Club makes no guarantee of availability of the service.**

We will do our best to keep the service available at all times, and provide
advanced notice of service.

### Data Integrity

> **The Computer Science Club makes no guarantee of data integrity.**

It is your responsibility to ensure that you have appropriate backups of all
data stored on the service. We are not responsible for any lost data
whatsoever.

## Auditing and Logging

The Computer Science Club Systems Committee keeps records ("logs") on the
service to:

1. Collect and store diagnostic information to aid in debugging hardware and
   software issues;
2. Document manchine usage and activity;
3. Refer to for auditing purposes.

### Network Address Translation

The Computer Science Club uses Network Address Translation on IPv4 to allow
access to off-campus resources. All translations performed are logged with
source IP, destination IP, source port, destination port and timestamps
indicating the start and end of the translation.

### Use of Records

The logs contain records of user and machine activity. The Systems Committee
reserves the right to share these records with the University or local
law enforcement as required.

## User Agreement

I have read and understood the usage agreement of 16 August 2017, and I
agreee that my usage of the Computer Science Club will be in accordance with
this policy. I understand that I am responsible for all actions performed from
my account (or the services attached to it). Furthermore, I accept full legal
responsibility for all of the actions that I commit using the Computer Science
Club Cloud according to any and all applicable laws.

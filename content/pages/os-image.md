title: Adding a New OS Image
slug: docs/admin/os-image
status: hidden

## Adding an Ubuntu Image

Use the daily builds for the Ubuntu Cloud Server images. These can be found
[here][ubuntu].

### 1. Download the target image

On `corn-syrup.csclub.uwaterloo.ca`, download your target image.  Ensure you
pick a qcow2 formatted image. These usually end in the `*.img` extension.

For example,

```sh
cd /tmp
wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
```

### 2. Upload the image to OpenStack

Note that you must have set your OpenStack environment variables correctly for
this step to work. See Appendix A below.

Make sure to set your `$FILENAME` and `$IMAGE_NAME` variables to the location
and name of your desired image. For example,

```sh
FILENAME="/tmp/bionic-server-cloudimg-amd64.img"
IMAGE_NAME="Ubuntu Server 18.04 LTS (Bionic Beaver) daily build"
openstack image create --container-format bare --disk-format qcow2 --file $FILENAME --public $IMAGE_NAME
```

### Appendix A: OpenStack Environment Variables

```sh
export OS_USERNAME=<username>
export OS_PROJECT_NAME=csc
export OS_USER_DOMAIN_NAME=csclub
export OS_PROJECT_DOMAIN_NAME=csclub
export OS_AUTH_URL=https://auth.cloud.csclub.uwaterloo.ca/v3
export OS_IDENTITY_API_VERSION=3
```

NOTE: To control your personal project, change `OS_PROJECT_NAME` to your username.

[ubuntu]: https://cloud-images.ubuntu.com/

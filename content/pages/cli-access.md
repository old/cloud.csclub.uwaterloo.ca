title: Command Line Access
slug: docs/cli-access
status: hidden

The OpenStack command line interface has been installed on corn-syrup.

Set the following environment variables to authenticate with the CSC Cloud:

```bash
export OS_USERNAME=<username>
export OS_PROJECT_NAME=<username>
export OS_USER_DOMAIN_NAME=csclub
export OS_PROJECT_DOMAIN_NAME=csclub
export OS_AUTH_URL=https://auth.cloud.csclub.uwaterloo.ca/v3
export OS_IDENTITY_API_VERSION=3
```

Documentation on the command line is available from the OpenStack project:
[OpenStackClient](https://docs.openstack.org/python-openstackclient/latest/).

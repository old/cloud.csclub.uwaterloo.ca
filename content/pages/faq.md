title: Frequently Asked Questions
slug: faq
status: hidden

## Networking

### What IP ranges are available?

When you create a server on the `MSO Intranet` network, you are assigned an
IPv4 address in the range of `172.19.134.0/24` and an IPv6 address in the range
of `2620:101:f000:4903::/64`.

### How can I access my server?

To access your server you must be connected to the University's network. This
can be done via wired, wireless (Eduroam) or the
[VPN](https://uwaterloo.ca/information-systems-technology/services/virtual-private-network-vpn).

### I'm assigned a globally-routed IPv6 address. Why can't I access it from the internet?

All traffic to your server initiated from off-campus is blocked at the
University's border firewall. We are not accepting requests for exceptions at
this time.

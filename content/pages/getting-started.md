title: Getting Started
slug: docs/getting-started
status: hidden

## Prerequisites

- **Beta**: You must request and be granted access from the <a
  href="mailto:cloud@csclub.uwaterloo.ca">cloud@csclub.uwaterloo.ca</a> to use the
  Computer Science Club Cloud. *Please include your CSC username when
  requesting access*.

## Accessing the Dashboard

1. Go to [Computer Science Club Cloud
   Dashboard](https://dashboard.cloud.csclub.uwaterloo.ca/)
2. Login with your Computer Science Club username and password

## Creating a server

1. On the Dashboard, open `Project` &#8594; `Compute` &#8594; `Instances`.
2. Click `Launch Instance`
3. Fill in instance details
	- Select a name
	- Choose `csc-mc` availability zone
4. Click next. Select an image
5. Click next. Choose a flavour
6. Click next. Select a network
	- Select `MSO Intranet` (unless you are working with private networking,
	  see the Advanced Network Guide)
7. Click on `Key Pair`
	- If you have not already uploaded a SSH key pair, upload your public key
	- *This step is important. Without a SSH key, you will not be able to
	  access your VM.*
8. Click `Launch Instance`

> *If* everything works correctly, your VM *should* be provisioned.

## Accessing the server

The dashboard will show the IP addresses assigned to the VM. You can SSH
using the corresponding SSH private key.

The default username is typically the name of the distribution (e.g., `ubuntu`
for Ubuntu and `debian` for Debian).


**You cannot connect to your VM from the internet. You must connect by SSH'ing
from a CSC machine or by using on-campus ethernet, wireless or the
[Campus VPN](https://uwaterloo.ca/information-systems-technology/services/virtual-private-network-vpn).**

You may wish to configure an account password (using `passwd`) to allow access
via the console (see the next section).

## Oh no! I broke SSH / Networking / etc.

You can access the console through the dashboard. View the instance details (by
clicking its name) and then click on the `Console` tab.

The console may not accept keyboard / mouse input. If so, click on `Click here
to show only console` to access the fullscreen view.

## Mirrors

If possible, you should use
[mirror.csclub.uwaterloo.ca](http://mirror.csclub.uwaterloo.ca) as your mirror.

## Web Hosting

As you may have noticed, you cannot access your VM from the internet. To allow
access to your website, you have two options:

1. Proxy through `caffeine`. See the [CSC
	wiki](https://wiki.csclub.uwaterloo.ca/Web_Hosting#Reverse_Proxy_.28Python.2C_Ruby.2C_Perl.2C_etc..29)
	for more information
2. Configure on the CSC HAProxy load balancer. Contact
	cloud@csclub.uwaterloo.ca with your domain name, username, instance IP
	addresses and ports (or DNS hostname). TLS certificates are currently only
	available for `*.csclub.cloud` domains (contact cloud@csclub.uwaterloo.ca to
	register a `[yourusername-blah].csclub.cloud` hostname).



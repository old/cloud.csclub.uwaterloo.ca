title: Documentation
slug: docs

## For users

- [Getting Started]({filename}getting-started.md)
- [CLI Access]({filename}cli-access.md)
- [Advanced Networking]({filename}advanced-networking.md)

A great resource is the [OpenStack user
guides](https://docs.openstack.org/ocata/user/) (Note: we are currently running
the Ocata version).

## For administrators

- [User Setup]({filename}user-setup.md)
- [Adding a New OS Image]({filename}os-image.md)

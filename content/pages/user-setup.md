title: User Setup
slug: docs/admin/user-setup
status: hidden

## Option 1 (Recommended)

Use the `adduser.py` script, available at [https://git.uwaterloo.ca/zachary.seguin/csclub-cloud-scripts](https://git.uwaterloo.ca/zachary.seguin/csclub-cloud-scripts).

## Option 2

### 1. LDAP Configuration

A `userPassword` property must be added to a user's LDAP object before they can
successfully authenticate against our cloud environment.

1. Open ldapvi `ldapvi -Y GSSAPI`
2. Find the user, and add: `userPassword: {SASL}<username>@CSCLUB.UWATERLOO.CA`
3. Quit your editor
4. Type `y` to perform the changes

### 2. Add user project

All members will be assigned their own project. All instructions are executed
using the OpenStack command line client.

1. `openstack project create --domain csclub <username>`
2. `openstack role --project <username> --user <username> user`

### Appendix A: OpenStack Environment Variables

```sh
export OS_USERNAME=<username>
export OS_PASSWORD=<password>
export OS_PROJECT_NAME=csc
export OS_USER_DOMAIN_NAME=csclub
export OS_PROJECT_DOMAIN_NAME=csclub
export OS_AUTH_URL=https://auth.cloud.csclub.uwaterloo.ca/v3
export OS_IDENTITY_API_VERSION=3
```

NOTE: To control your project, change `OS_PROJECT_NAME` to your username.

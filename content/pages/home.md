Title: CSC Cloud (Beta)
URL:
save_as: index.html
sort: 0

The [Computer Science Club](https://csclub.uwaterloo.ca) is currently working on a private cloud environment (powered by [OpenStack](https://www.openstack.org)) to enhance our web hosting services. This will allow the club to offer private servers to our members.

## Thanks to our donors

Hardware for this project was provided by:

- Funding from the [Mathematics Endowment Fund](https://uwaterloo.ca/math-endowment-fund/)
- Funding from the [Student Life Endowment Fund](https://feds.ca/funding#fund-slef)
- Hardware donations from club members
